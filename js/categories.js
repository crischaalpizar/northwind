var _list = [];

function LoadCategories(){

    var settings = {
        url: "https://northwind.vercel.app/api/categories", 
        success: function(result, status, xhr){
            //alert(status)
            _list = result;
            console.log(_list);
            //PrintCategories();

            $.each(_list, PrintCategories);
        },
        error: function(){
            alert('Error');
        }
    };

    $.ajax(settings);
}

var PrintCategories = function(index, value){
    $('#categoriesBody').append(
        `<tr>
            <td>
                <a href="/northwind/category.html?id=${value.id}">
                    ${value.name}
                </a>
            </td>
            <td>
                ${value.description}
            </td>
        </tr>`
    );
}

$(document).ready(function(){
    LoadCategories();
});